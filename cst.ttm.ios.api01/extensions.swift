//
//  extensions.swift
//  cst.ttm.ios.api01
//
//  Created by Frank Schäfer on 09.09.19.
//  Copyright © 2019 Frank Schäfer. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var isValidURL: Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count)) {
            // it is a link, if the match covers the whole string
            return match.range.length == utf16.count
        } else {
            return false
        }
    }
}


extension UIImage
{
   
    func makeImageWithColor(_ color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }

}


extension UIColor
{

    var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)? {
        var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        return getRed(&r, green: &g, blue: &b, alpha: &a) ? (r,g,b,a) : nil
    }
    
    func jetColor(value : Double,
                  vmin : Double,
                  vmax : Double) -> UIColor
    {
        var red : Double = 1
        var green : Double = 1
        var blue : Double = 1

        var v : Double = value
        
        if (v < vmin)
        {
           v = vmin;
        }
        if (v > vmax)
        {
           v = vmax;
        }
        
        let dv = vmax - vmin;
        
        if (v < (vmin + 0.25 * dv)) {
            red = 0
            green = 4 * (v - vmin) / dv;
         } else if (v < (vmin + 0.5 * dv)) {
            red = 0;
            blue = 1 + 4 * (vmin + 0.25 * dv - v) / dv;
         } else if (v < (vmin + 0.75 * dv)) {
            red = 4 * (v - vmin - 0.5 * dv) / dv;
            blue = 0;
         } else {
            green = 1 + 4 * (vmin + 0.75 * dv - v) / dv;
            blue = 0;
         }

        return UIColor(red: CGFloat(red),
                       green: CGFloat(green),
                       blue: CGFloat(blue),
                       alpha: 1)
    }

    func redToGreedColor(value : Double,
                         vmin : Double,
                         vmax : Double) -> UIColor
    
    {
        let middle = (vmin + vmax) / 2
        let scale = 1 / ( middle - vmin );
        
        if value <= vmin
        {
            return UIColor.init(red: 1, green: 0, blue: 0, alpha: 1) // "FF0000"
        }
        //  upper boundary
        if value >= vmax
        {
            return UIColor.init(red: 0, green: 1, blue: 0, alpha: 1) // "00FF00"
        }
        
        if value < middle
        {
            let green = CGFloat((value - vmin) * scale)
            return UIColor.init(red: 1, green: green, blue: 0, alpha: 1)
        }
        else
        {
            let red = 1 - CGFloat( ( value - middle ) * scale )
             return UIColor.init(red: red, green: 1, blue: 0, alpha: 1)
        }
    }
    
}
