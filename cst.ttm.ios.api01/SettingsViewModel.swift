//
//  ProfileViewModel.swift
//  TableViewWithMultipleCellTypes
//
// (C) Frank Schäfer, CST GmbH, Germany Waldshut
//

import Foundation
import UIKit

enum SettingsViewModelItemType {
    case meta
    case siteInfo
    case siteLocation
    case siteSettings
}


protocol SettingsViewModelItem {
    var type: SettingsViewModelItemType { get }
    var sectionTitle: String { get }
    var rowCount: Int { get }
}


class SettingsViewModel: NSObject {
    
    var items = [SettingsViewModelItem]()

    override init() {
        super.init()

        guard let theData = dataFromTTM,
              let theCrossings = Crossings(data: theData) else {
            return
        }
        
        guard let theDataType = theCrossings.meta?.dataType,
              let theTimeStamp = theCrossings.meta?.timeStamp,
              let theTimeZone = theCrossings.meta?.timeZone,
              let theTTMSerialNumber = theCrossings.meta?.ttmSerialNumber,
              let theTTMVersion = theCrossings.meta?.ttmVersion,
              let theUnitOfMeasurement = theCrossings.meta?.unitOfMeasurement else {
            return
        }
    
        
        let metaItem = SettingsViewModelMetaItem.init(dataTypeValue: theDataType,
                                                      timeStampValue: theTimeStamp,
                                                      timeZoneValue: theTimeZone,
                                                      ttmSerialNumberValue: theTTMSerialNumber,
                                                      ttmVersionValue: theTTMVersion,
                                                      unitOfMeasurementValue: theUnitOfMeasurement)
        
        items.append(metaItem)

        
        guard let theInfoLine1 = theCrossings.site?.infoLine1,
              let theInfoLine2 = theCrossings.site?.infoLine2 else {
            return
        }
        
        let siteInfoItem = SettingsViewModelSiteInfoItem.init(infoLine1: theInfoLine1,
                                                              infoLine2: theInfoLine2)
        
        items.append(siteInfoItem)


        guard let theLocationLine1 = theCrossings.site?.locationLine1,
              let theLocationLine2 = theCrossings.site?.locationLine2,
              let theLocationLine3 = theCrossings.site?.locationLine3,
              let theLocationLine4 = theCrossings.site?.locationLine4,
              let theLocationLine5 = theCrossings.site?.locationLine5 else {
            return
        }
        
        let siteInfoLocation = SettingsViewModelSiteLocationItem.init(locationLine1: theLocationLine1,
                                                                  locationLine2: theLocationLine2,
                                                                  locationLine3: theLocationLine3,
                                                                  locationLine4: theLocationLine4,
                                                                  locationLine5: theLocationLine5)
        
        items.append(siteInfoLocation)

        
        guard let theAutoPrint = theCrossings.site?.setting?.autoPrint,
              let theDisplayDurationOfResults = theCrossings.site?.setting?.displayDurationOfResults,
              let theMinimumLimitSummer = theCrossings.site?.setting?.minimumLimitSummer,
              let theMinimumLimitWinter = theCrossings.site?.setting?.minimumLimitWinter,
              let theRecommendedLimitSummer = theCrossings.site?.setting?.recommendedLimitSummer,
              let theRecommendedLimitWinter = theCrossings.site?.setting?.recommendedLimitWinter,
              let theUnitOfMeasurementForDisplay = theCrossings.site?.setting?.unitOfMeasurementForDisplay,
              let theUseWinterTireLimit = theCrossings.site?.setting?.useWinterTireLimit,
              let theVehicleMovementForDisplay = theCrossings.site?.setting?.vehicleMovementForDisplay else {
            return
        }
        
        let siteSetting = SettingsViewModelSiteSittingsItem.init(autoPrintValue: theAutoPrint,
                                                                 displayDurationOfResultsValue: String(theDisplayDurationOfResults),
                                                                 minimumLimitSummerValue: String(format: "%.1f", theMinimumLimitSummer),
                                                                 minimumLimitWinterValue: String(format: "%.1f", theMinimumLimitWinter),
                                                                 recommendedLimitSummerValue: String(format: "%.1f", theRecommendedLimitSummer),
                                                                 recommendedLimitWinterValue: String(format: "%.1f", theRecommendedLimitWinter),
                                                                 unitOfMeasurementForDisplayValue: theUnitOfMeasurementForDisplay,
                                                                 useWinterTireLimitValue: theUseWinterTireLimit.description,
                                                                 vehicleMovementForDisplayValue: theVehicleMovementForDisplay)
        
        items.append(siteSetting)


        
    }
}

extension SettingsViewModel: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].rowCount
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = items[indexPath.section]
        switch item.type {
        case .meta:
            if let cell = tableView.dequeueReusableCell(withIdentifier: MetaCell.identifier,
                                                                   for: indexPath) as? MetaCell {
                           cell.item = item
                           return cell
                       }
        case .siteInfo:
            if let cell = tableView.dequeueReusableCell(withIdentifier: SiteInfoCell.identifier,
                                                        for: indexPath) as? SiteInfoCell {
                cell.item = item
                return cell
            }

        case .siteLocation:
            if let cell = tableView.dequeueReusableCell(withIdentifier: SiteLocationCell.identifier,
                                                        for: indexPath) as? SiteLocationCell {
                cell.item = item
                return cell
            }

        case .siteSettings:
            if let cell = tableView.dequeueReusableCell(withIdentifier: SiteSettingsCell.identifier,
                                                        for: indexPath) as? SiteSettingsCell {
                cell.item = item
                return cell
            }

        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView,
                   titleForHeaderInSection section: Int) -> String? {
        return items[section].sectionTitle
    }
}



class SettingsViewModelMetaItem: SettingsViewModelItem {
    
    var type: SettingsViewModelItemType {
        return .meta
    }

    var sectionTitle: String {
        return "Meta"
    }

    var rowCount: Int {
        return 1
    }

    var dataTypeValue: String
    var timeStampValue : String
    var timeZoneValue: String
    var ttmSerialNumberValue : String
    var ttmVersionValue: String
    var unitOfMeasurementValue : String

    init(dataTypeValue: String,
         timeStampValue : String,
         timeZoneValue: String,
         ttmSerialNumberValue : String,
         ttmVersionValue : String,
         unitOfMeasurementValue : String) {
        self.dataTypeValue = dataTypeValue
        self.timeStampValue = timeStampValue
        self.timeZoneValue = timeZoneValue
        self.ttmSerialNumberValue = ttmSerialNumberValue
        self.ttmVersionValue = ttmVersionValue
        self.unitOfMeasurementValue = unitOfMeasurementValue
    }
}


class SettingsViewModelSiteInfoItem: SettingsViewModelItem {
    
    var type: SettingsViewModelItemType {
        return .siteInfo
    }

    var sectionTitle: String {
        return "Site info"
    }

    var rowCount: Int {
        return 1
    }

    var infoLine1: String
    var infoLine2 : String

    init(infoLine1: String,
         infoLine2 : String) {
        self.infoLine1 = infoLine1
        self.infoLine2 = infoLine2
    }
}


class SettingsViewModelSiteLocationItem: SettingsViewModelItem {
    
    var type: SettingsViewModelItemType {
        return .siteLocation
    }

    var sectionTitle: String {
        return "Site location"
    }

    var rowCount: Int {
        return 1
    }

    var line1: String
    var line2 : String
    var line3: String
    var line4 : String
    var line5: String

    init(locationLine1: String,
         locationLine2 : String,
         locationLine3 : String,
         locationLine4 : String,
         locationLine5 : String) {
        self.line1 = locationLine1
        self.line2 = locationLine2
        self.line3 = locationLine3
        self.line4 = locationLine4
        self.line5 = locationLine5
    }
}

class SettingsViewModelSiteSittingsItem: SettingsViewModelItem {
    
    var type: SettingsViewModelItemType {
        return .siteSettings
    }

    var sectionTitle: String {
        return "Site settings"
    }

    var rowCount: Int {
        return 1
    }

    var autoPrint: String
    var displayDurationOfResultsValue : String
    var minimumLimitSummerValue: String
    var minimumLimitWinterValue : String
    var recommendedLimitSummerValue: String
    var recommendedLimitWinterValue: String
    var unitOfMeasurementForDisplayValue: String
    var useWinterTireLimitValue: String
    var vehicleMovementForDisplayValue: String

    init(autoPrintValue: String,
         displayDurationOfResultsValue : String,
         minimumLimitSummerValue : String,
         minimumLimitWinterValue : String,
         recommendedLimitSummerValue : String,
         recommendedLimitWinterValue : String,
         unitOfMeasurementForDisplayValue : String,
         useWinterTireLimitValue : String,
         vehicleMovementForDisplayValue : String)
    {
        self.autoPrint = autoPrintValue
        self.displayDurationOfResultsValue = displayDurationOfResultsValue
        self.minimumLimitSummerValue = minimumLimitSummerValue
        self.minimumLimitWinterValue = minimumLimitWinterValue
        self.recommendedLimitSummerValue = recommendedLimitSummerValue
        self.recommendedLimitWinterValue = recommendedLimitWinterValue
        self.unitOfMeasurementForDisplayValue = unitOfMeasurementForDisplayValue
        self.useWinterTireLimitValue = useWinterTireLimitValue
        self.vehicleMovementForDisplayValue = vehicleMovementForDisplayValue
    }
}

