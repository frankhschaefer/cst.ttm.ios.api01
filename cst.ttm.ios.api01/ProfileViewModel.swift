//
//  ProfileViewModel.swift
//  TableViewWithMultipleCellTypes
//
// (C) Frank Schäfer, CST GmbH, Germany Waldshut
//

import Foundation
import UIKit

enum ProfileViewModelItemType {
    case nameAndPicture
    case crossingStatus
    case crossingDirection
    case timeStamp
    case treadDepthItem
    case plate
}

protocol ProfileViewModelItem {
    var type: ProfileViewModelItemType { get }
    var sectionTitle: String { get }
    var rowCount: Int { get }
}

class ProfileViewModel: NSObject {
    
    var items = [ProfileViewModelItem]()

    override init() {
        super.init()

        guard let theData = dataFromTTM,
              let theCrossings = Crossings(data: theData) else {
            return
        }

        _ = theCrossings.cars.map { car in

            if let name = car.name,
               let carColor = car.carColor {
                let nameAndPictureItem = ProfileViewModelNamePictureItem(name: name,
                                                                         carColor: carColor)
                items.append(nameAndPictureItem)
            }

            let crossingStatusItem = ProfileViewModelCrossingStatusItem(crossingStatus: car.crossingStatusText)

            items.append(crossingStatusItem)

            if let theCrossingDirection = car.crossingDirection,
                let theName = theCrossingDirection.name,
                let theConfidence = theCrossingDirection.confidence,
                let thePictureURL = theCrossingDirection.pictureUrl {
                let crossingDirectionItem = ProfileViewModeCrossingDirectionItem(crossingsDriection: theName,
                                                                                 confidence: theConfidence,
                                                                                 pictureUrl: thePictureURL)
                items.append(crossingDirectionItem)
            }

            let timeStampItem = ProfileViewModelTimeStampItem(name: car.timestampString)

            items.append(timeStampItem)

            var tires : [TireData] = []
            for tirePosition in tirePositions {
                tires.append(TireData.init(car: car,
                                           tirePosition: tirePosition))
            }
            
            let treadDepthItem = ProfileViewModelTreadDepthItem.init(tires: tires)

            items.append(treadDepthItem)

            
            if let thePlateValue = car.plateValue,
                let thePlateConfidence = car.plateConfidence,
                let theCountryValue = car.countryValue,
                let theCountryConfidence = car.countryConfidence {
                let plateItem = ProfileViewModelPlateItem(country: thePlateValue,
                                                          countryConfidence: thePlateConfidence,
                                                          plate: theCountryValue,
                                                          plateConfidence: theCountryConfidence)

                items.append(plateItem)
            }
        }

    }
}



extension ProfileViewModel: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].rowCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        switch item.type {
        case .nameAndPicture:
            if let cell = tableView.dequeueReusableCell(withIdentifier: NamePictureCell.identifier,
                                                        for: indexPath) as? NamePictureCell {
                cell.item = item
                return cell
            }
        case .crossingStatus:
            if let cell = tableView.dequeueReusableCell(withIdentifier: CrossingStatus.identifier,
                                                        for: indexPath) as? CrossingStatus {
                cell.item = item
                return cell
            }
        case .treadDepthItem:
            if let item = item as? ProfileViewModelTreadDepthItem,
               let cell = tableView.dequeueReusableCell(withIdentifier: TreadDepthCell.identifier,
                                                        for: indexPath) as? TreadDepthCell {
                let tireData = item.tires[indexPath.row]
                cell.item = tireData
                return cell
            }
        case .timeStamp:
            if let cell = tableView.dequeueReusableCell(withIdentifier: TimeStampCell.identifier,
                                                        for: indexPath) as? TimeStampCell {
                cell.item = item
                return cell
            }
        case .crossingDirection:
            if let cell = tableView.dequeueReusableCell(withIdentifier: CrossingDirectionCell.identifier,
                                                        for: indexPath) as? CrossingDirectionCell {
                cell.item = item
                return cell
            }
        case .plate:
            if let cell = tableView.dequeueReusableCell(withIdentifier: PlateCell.identifier,
                                                        for: indexPath) as? PlateCell {
                cell.item = item
                return cell
            }
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return items[section].sectionTitle
    }
}


class ProfileViewModelNamePictureItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .nameAndPicture
    }

    var sectionTitle: String {
        return "Overview information about the car"
    }

    var rowCount: Int {
        return 1
    }

    var name: String

    var carColor: UIColor

    init(name: String,
         carColor: UIColor) {
        self.name = name
        self.carColor = carColor
    }
}

class ProfileViewModelCrossingStatusItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .crossingStatus
    }

    var sectionTitle: String {
        return "Crossingstatus"
    }

    var rowCount: Int {
        return 1
    }

    var crossingStatus: String

    init(crossingStatus: String) {
        self.crossingStatus = crossingStatus
    }
}

class ProfileViewModelPlateItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .plate
    }

    var sectionTitle: String {
        return "Licence plate"
    }

    var rowCount: Int {
        return 1
    }

    var country: String
    var countryConfidence: Int
    var plate: String
    var plateConfidence: Int

    init(country: String,
         countryConfidence: Int,
         plate: String,
         plateConfidence: Int) {
        self.country = country
        self.countryConfidence = countryConfidence
        self.plate = plate
        self.plateConfidence = plateConfidence
    }
}

class ProfileViewModelTimeStampItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .timeStamp
    }

    var sectionTitle: String {
        return "Timestamp"
    }

    var rowCount: Int {
        return 1
    }

    var name: String

    init(name: String) {
        self.name = name
    }
}


class ProfileViewModeCrossingDirectionItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .crossingDirection
    }

    var sectionTitle: String {
        return "Crossing Direction"
    }

    var rowCount: Int {
        return 1
    }

    var pictureUrl: String

    var crossingsDirection: String

    var confidence: Int

    init(crossingsDriection: String,
         confidence: Int,
         pictureUrl: String) {
        self.confidence = confidence
        crossingsDirection = crossingsDriection
        self.pictureUrl = pictureUrl
    }
}


class TireData {
    var mainTD: Float
    var leftRTD: Float
    var middleRTD: Float
    var rightRTD: Float
    var mainCD: Int
    var leftCD: Int
    var middleCD: Int
    var rightCD: Int
    var tirePosition: String
    
    init(car: Car,
         tirePosition: String) {
        mainTD = car.getMainTreadDepthFromTire(tirePosition: tirePosition) ?? -1

        leftRTD = car.geRegionTreadDepthValueFromTire(tirePosition: tirePosition, region: regionPositions[0]) ?? -1
        middleRTD = car.geRegionTreadDepthValueFromTire(tirePosition: tirePosition, region: regionPositions[1]) ?? -1
        rightRTD = car.geRegionTreadDepthValueFromTire(tirePosition: tirePosition, region: regionPositions[2]) ?? 1

        self.tirePosition = tirePosition

        mainCD = car.getMainConfidenceFromTire(tirePosition: tirePosition) ?? -1
        
        leftCD = car.geRegionConfidenceValueFromTire(tirePosition: tirePosition, region: regionPositions[0]) ?? -1
        middleCD = car.geRegionConfidenceValueFromTire(tirePosition: tirePosition, region: regionPositions[1]) ?? -1
        rightCD = car.geRegionConfidenceValueFromTire(tirePosition: tirePosition, region: regionPositions[2]) ?? -1
    }
}

class ProfileViewModelTreadDepthItem: ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .treadDepthItem
    }

    var sectionTitle: String {
        return "Tread Depth"
    }

    var rowCount: Int {
        return tires.count
    }

    var tires : [TireData]

    init(tires: [TireData]) {
        self.tires = tires
    }
}

