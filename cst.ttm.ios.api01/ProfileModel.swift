//
//  ProfileModel.swift
//  TableViewWithMultipleCellTypes
// (C) Frank Schäfer, CST GmbH, Germany Waldshut


import Foundation
import SwiftDate
import SwiftyJSON

var _statusDemoLive : Bool = true

/// Contains the status true, until the first access to the IP-Adress was succesfully
public var statusDemoLive : Bool
{
    get {
        return _statusDemoLive
    }
    set {
        if newValue != _statusDemoLive
        {
            _statusDemoLive = newValue
        }
    }
}


public func dataFromFile(_ filename: String) -> Data? {
    @objc class TestClass: NSObject {}

    let bundle = Bundle(for: TestClass.self)

    if let path = bundle.path(forResource: filename, ofType: "json") {
        return (try? Data(contentsOf: URL(fileURLWithPath: path)))
    }

    return nil
}


/// Stores the var JSON-STring from the TTM
var _dataFromTTM : Data?

/// Handle the data from the file or from the TTM
public var dataFromTTM : Data?
{
    get
    {
        guard let theDataFromTTM = _dataFromTTM else
        {
            return dataFromFile("ttm")
        }
        
        return theDataFromTTM
    }

    set
    {
        if newValue  != _dataFromTTM
        {
            _dataFromTTM = newValue
        }
    }
}



class Crossings {
    
    var cars = [Car]()
    
    var meta : Meta?
    
    var site : Site?

    init?(data: Data) {
        do {
            let json = try JSON(data: data)

            var carNo: Int = 0

            for crossing in json["crossings"] {
                if let theCar = Car(crossing: crossing.1,
                                    number: carNo) {
                    cars.append(theCar)
                    carNo += 1
                }
            }
            
            meta = Meta(meta: json["meta"])

            site = Site(site: json["site"])

        } catch {
            print("Error deserializing JSON: \(error)")
        }
    }
}


class Settings
{
    /*
    "settings" : {
      "autoPrint" : "never",
      "displayDurationOfResults" : 0,
      "minimumLimitSummer" : 3,
      "minimumLimitWinter" : 3,
      "recommendedLimitSummer" : 3.7000000476837158,
      "recommendedLimitWinter" : 4,
      "unitOfMeasurementForDisplay" : "mm",
      "useWinterTireLimit" : false,
      "vehicleMovementForDisplay" : "both"
    */
    
    var autoPrint : String?
    
    var displayDurationOfResults : Int?
    var minimumLimitSummer  : Float?
    var minimumLimitWinter  : Float?
    var recommendedLimitSummer  : Float?
    var recommendedLimitWinter  : Float?
    var unitOfMeasurementForDisplay  : String?
    var useWinterTireLimit : Bool?
    var vehicleMovementForDisplay  : String?
    
    
    init?(settings: JSON) {

        guard let theAutoPrint = settings["autoPrint"].string,
              let theDisplayDurationOfResults = settings["displayDurationOfResults"].int,
              let theMinimumLimitSummer = settings["minimumLimitSummer"].float,
              let theMinimumLimitWinter = settings["minimumLimitWinter"].float,
              let theRecommendedLimitSummer = settings["recommendedLimitSummer"].float,
              let theRecommendedLimitWinter = settings["recommendedLimitWinter"].float,
              let theUnitOfMeasurementForDisplay = settings["unitOfMeasurementForDisplay"].string,
              let theUseWinterTireLimit = settings["useWinterTireLimit"].bool,
              let theVehicleMovementForDisplay = settings["vehicleMovementForDisplay"].string else {
            return nil
        }
        
        autoPrint = theAutoPrint
        displayDurationOfResults = theDisplayDurationOfResults
        minimumLimitSummer = theMinimumLimitSummer
        minimumLimitWinter = theMinimumLimitWinter
        recommendedLimitSummer = theRecommendedLimitSummer
        recommendedLimitWinter = theRecommendedLimitWinter
        unitOfMeasurementForDisplay = theUnitOfMeasurementForDisplay
        useWinterTireLimit = theUseWinterTireLimit
        vehicleMovementForDisplay = theVehicleMovementForDisplay
    }
}
    


class Site {
    
    /*
     "site" : {
     "info" : {
       "line1" : "",
       "line2" : ""
     },
     "location" : {
       "line1" : "",
       "line2" : "",
       "line3" : "",
       "line4" : "",
       "line5" : ""
     },
     */
    
    var infoLine1 : String?
    var infoLine2 : String?

    var locationLine1 : String?
    var locationLine2 : String?
    var locationLine3 : String?
    var locationLine4 : String?
    var locationLine5 : String?

    var setting : Settings?
    
    init?(site: JSON) {

        guard let theInfoLine1 = site["info"]["line1"].string,
              let theInfoLine2 = site["info"]["line2"].string,
              let theLocationLine1 = site["location"]["line1"].string,
              let theLocationLine2 = site["location"]["line2"].string,
              let theLocationLine3 = site["location"]["line3"].string,
              let theLocationLine4 = site["location"]["line4"].string,
              let theLocationLine5 = site["location"]["line5"].string else {
            return nil
        }

        setting = Settings(settings: site["settings"])

        infoLine1 = theInfoLine1

        infoLine2 = theInfoLine2

        locationLine1 = theLocationLine1
        
        locationLine2 = theLocationLine2
        
        locationLine3 = theLocationLine3
        
        locationLine4 = theLocationLine4

        locationLine5 = theLocationLine5
    }

}



class Meta {
    /*
    "dataType" : "TTM\/WebAPI\/v3",
    "timeStamp" : "19-10-10 05:19:57.227",
    "timeZone" : "+0200",
    "ttmSerialNumber" : "0000562",
    "ttmVersion" : "1.92.0 RC",
    "unitOfMeasurement" : "mm"
     */
    
    var dataType: String?
    
    var timeStamp : String?

    var timeZone : String?

    var ttmSerialNumber : String?
    
    var ttmVersion : String?
    
    var unitOfMeasurement : String?

    init?(meta: JSON) {

        guard let theDataType = meta["dataType"].string,
              let theTimeStamp = meta["timeStamp"].string,
              let theTimeZone = meta["timeZone"].string,
              let theTTMSerialNumber = meta["ttmSerialNumber"].string,
              let theTTMVersion = meta["ttmVersion"].string,
              let theUnitOfmeasurement = meta["unitOfMeasurement"].string else {
            return nil
        }

        dataType = theDataType

        timeStamp = theTimeStamp

        timeZone = theTimeZone
        
        ttmSerialNumber = theTTMSerialNumber
        
        ttmVersion = theTTMVersion
        
        unitOfMeasurement = theUnitOfmeasurement
    }

}


class Car {
    var crossingDirection: CrossingDirection?
    var crossingStatus: Int?
    var timestamp: DateInRegion?
    var tires: Tires?
    var vehicleID: VehicleID?
    var number: Int
    var profileAttributes = [Attribute]()
    var carColor : UIColor?
    var name: String?

    public var minTreadDepth: Tire? {
        return tires?.tires.min(by: { $0.treadDepthValue ?? 0.0 < $1.treadDepthValue ?? 0.0 })
    }

    public var maxTreadDepth: Tire? {
        return tires?.tires.max(by: { $0.treadDepthValue ?? 0.0 < $1.treadDepthValue ?? 0.0 })
    }

    func getMainTreadDepthFromTire(tirePosition: String) -> Float? {
        return getTireFromPosition(tirePosition: tirePosition)?.treadDepthValue
    }

    func getMainConfidenceFromTire(tirePosition: String) -> Int? {
        return getTireFromPosition(tirePosition: tirePosition)?.treadDepthConfidence
    }

    func geRegionTreadDepthValueFromTire(tirePosition: String,
                                         region: String) -> Float? {
        return getRegionFromTire(tirePosition: tirePosition, region: region)?.value
    }

    func geRegionConfidenceValueFromTire(tirePosition: String,
                                         region: String) -> Int? {
        return getRegionFromTire(tirePosition: tirePosition, region: region)?.confidence
    }

    func getRegionFromTire(tirePosition: String, region: String) -> Region? {
        return getTireFromPosition(tirePosition: tirePosition)?.regions.first(where: { $0.regionPosition == region })
    }

    func getTireFromPosition(tirePosition: String) -> Tire? {
        return tires?.tires.first(where: { $0.regionPosition == tirePosition })
    }

    var plateValue : String?
    {
        return vehicleID?.plateValue
    }

    var plateConfidence : Int?
    {
        return vehicleID?.plateConfidence
    }

    var countryValue : String?
    {
        return vehicleID?.countryValue
    }

    var countryConfidence : Int?
    {
        return vehicleID?.countryConfidence
    }

    
    var crossingStatusText: String {
        guard let theCrossingStatus = crossingStatus else {
            return "Crossingstatus undefiend"
        }

        switch theCrossingStatus {
        case -1:
            return "speed to low"
        case 0:
            return "Valid crossing"
        case 1:
            return "Speed to high"
        default:
            return "Crossingstatus undefiend"
        }
    }

    public var timestampString: String {
        return timestamp?.toString() ?? "unknown timestamp"
    }

    init?(crossing: JSON,
          number: Int) {
        guard let theCrossingDirection = CrossingDirection(crossingDirection: crossing["crossingDirection"]),
            let theTires = Tires(tires: crossing["tires"]),
            let theCrossingStatus = crossing["crossingStatus"].int,
            let theCrossingTimestamp = crossing["timestamp"].string else {
            return nil
        }

        self.number = number

        crossingDirection = theCrossingDirection

        crossingStatus = theCrossingStatus

        let defaultDate = DateInRegion("2010-01-01 00:00:00", region: SwiftDate.defaultRegion)!

        timestamp = theCrossingTimestamp.isEmpty ? defaultDate : theCrossingTimestamp.toISODate()

        tires = theTires

        vehicleID = VehicleID(vehicleID: crossing["vehicleID"])

        

        guard let theMin = minTreadDepth?.treadDepthValue,
            let theMax = maxTreadDepth?.treadDepthValue else {
            name = treadValueToShortText(value: -1,
                                         confidence: 0)
            return
        }

        carColor = UIColor().redToGreedColor(value: Double(theMin),
                                             vmin: 0,
                                             vmax: 8)
        
        name = treadValueToText(minValue: theMin,
                                maxValue: theMax)
    }
}

class Region {
    var name: String
    var pictureUrl: String
    var value: Float?
    var confidence: Int?
    var regionPosition: String

    init?(region: JSON,
          name: String) {
        regionPosition = name

        self.name = name + ", "

        guard let theConfidence = region["treadDepth"]["confidence"].int,
            let theValue = region["treadDepth"]["value"].float else {
            return nil
        }

        confidence = theConfidence

        value = theValue

        pictureUrl = treadValueToImage(value: theValue,
                                       confidence: theConfidence)

        self.name += treadValueToShortText(value: theValue,
                                           confidence: theConfidence)
    }
}

let tirePositions: [String] = ["frontLeft", "frontRight", "rearLeft", "rearRight"]

class Tires {
    var tires = [Tire]()

    var validTireCount: Int?

    init? (tires: JSON) {
        guard let theValidTireCount = tires["validTireCount"].int else {
            return nil
        }

        validTireCount = theValidTireCount

        for position in tirePositions {
            guard let theTire = Tire(tire: tires[position],
                                     position: position) else {
                return nil
            }

            self.tires.append(theTire)
        }
    }
}

let regionPositions: [String] = ["left", "middle", "right"]

class Tire {
    var name: String
    var pictureUrl: String?
    var crossingStatus: Int?
    var timeStamp: DateInRegion?
    var treadDepthConfidence: Int?
    var treadDepthValue: Float?
    var regions = [Region]()
    var regionPosition: String

    init? (tire: JSON,
           position: String) {
        regionPosition = position

        name = position + ", "

        crossingStatus = tire["crossingStatus"].int

        for position in regionPositions {
            if let theRegion = Region(region: tire["regions"][position],
                                      name: position) {
                regions.append(theRegion)
            }
        }

        timeStamp = (tire["timeStamp"].string ?? "").toISODate()

        treadDepthConfidence = tire["treadDepth"]["confidence"].int

        treadDepthValue = tire["treadDepth"]["value"].float

        pictureUrl = treadValueToImage(value: treadDepthValue,
                                       confidence: treadDepthConfidence)

        name += treadValueToShortText(value: treadDepthValue,
                                      confidence: treadDepthConfidence)
    }
}

class VehicleID {
    var name: String?
    var pictureUrl: String?

    var countryConfidence: Int?
    var countryValue: String?

    var plateConfidence: Int?
    var plateValue: String?

    var status: Int?

    init? (vehicleID: JSON) {
        guard let theCountryConfidence = vehicleID["country"]["confidence"].string,
            let theCountryValue = vehicleID["country"]["value"].string,
            let thePlateConfidence = vehicleID["plate"]["confidence"].string,
            let thePlatevalue = vehicleID["plate"]["value"].string,
            let theStatus = vehicleID["status"].int else {
            return nil
        }

        countryConfidence = Int(theCountryConfidence)
        countryValue = theCountryValue
        plateConfidence = Int(thePlateConfidence)
        plateValue = thePlatevalue
        status = theStatus

        name = "(" + theCountryValue + ") " + thePlatevalue

        pictureUrl = "plate"
    }
}

class CrossingDirection {
    var confidence: Int?
    var value: Int?

    var name: String?
    var pictureUrl: String?

    init?(crossingDirection: JSON) {
        guard let theValue = crossingDirection["value"].int,
            let theConfidence = crossingDirection["confidence"].int else {
            return nil
        }

        confidence = theConfidence
        value = theValue

        switch value ?? 0 {
        case -4 ... -1:
            pictureUrl = "arrowBackwards"
            name = "Backwards: " + String(value ?? -100)
        case 0:
            pictureUrl = "arrowUnclear"
            name = "Unclear: " + String(value ?? -100)
        case 1 ... 4:
            pictureUrl = "arrowForward"
            name = "Forwards: " + String(value ?? -100)
        default:
            pictureUrl = "arrowUnclear"
            name = "Unclear: " + String(value ?? -100)
        }
    }
}

class Attribute {
    var key: String?
    var value: String?

    init(json: [String: Any]) {
        key = json["key"] as? String
        value = json["value"] as? String
    }
}

func treadValueToImage(value: Float?,
                       confidence: Int?) -> String {
    switch value ?? -1 {
    case -1:
        return "car"
    case -1 ... 2:
        return "verybadcar"
    case 2 ... 3:
        return "badcar"
    case 3 ... 4:
        return "okaycar"
    case 4 ... 5:
        return "goodcar"
    case 5 ... 6:
        return "verygoodcar"
    case 6 ... 10:
        return "perfectcar"
    default:
        return "tom"
    }
}

func treadValueToShortText(value: Float?,
                           confidence: Int?) -> String {
    switch value ?? -1 {
    case -1:
        return "Unclear"
    case -1 ... 2:
        return "Very bad"
    case 2 ... 3.5:
        return "Bad"
    case 3.5 ... 5:
        return "Okay"
    case 5 ... 10:
        return "Very good"
    default:
        return "Unclear"
    }
}

func treadValueToText(minValue: Float,
                      maxValue: Float) -> String {
    let valueStr = String(format: ", Tread depth minimal value: %.2f mm", minValue) + " - " + String(format: "maximal value: %.2f mm", maxValue)

    switch minValue {
    case -1:
        return "Unclear" + valueStr
    case -1 ... 2:
        return "Very bad" + valueStr
    case 2 ... 3:
        return "Bad" + valueStr
    case 3 ... 4:
        return "Okay" + valueStr
    case 4 ... 5:
        return "Accecptable" + valueStr
    case 5 ... 6:
        return "Very good" + valueStr
    case 6 ... 19:
        return "Perfect" + valueStr
    default:
        return "Unclear" + valueStr
    }
}
