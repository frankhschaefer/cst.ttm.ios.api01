//
//  FirstViewController.swift
//  cst.ttm.ios.api01
//
//  Created by Frank Schäfer on 09.09.19.
//  Copyright © 2019 Frank Schäfer. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {
    
    fileprivate let viewModel = SettingsViewModel()

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView?.tag = 2
        
        tableView?.dataSource = viewModel

        tableView?.estimatedRowHeight = 100
        
        tableView?.rowHeight = UITableView.automaticDimension
        
        tableView?.register(MetaCell.nib, forCellReuseIdentifier: MetaCell.identifier)
        tableView?.register(SiteSettingsCell.nib, forCellReuseIdentifier: SiteSettingsCell.identifier)
        tableView?.register(SiteLocationCell.nib, forCellReuseIdentifier: SiteLocationCell.identifier)
        tableView?.register(SiteInfoCell.nib, forCellReuseIdentifier: SiteInfoCell.identifier)
    }

}
