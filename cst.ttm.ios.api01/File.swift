//
//  File.swift
//  cst.ttm.ios.api01
//
//  Created by Frank Schäfer on 10.09.19.
//  Copyright © 2019 Frank Schäfer. All rights reserved.
//

import Foundation

private var _ipAddress: String = ""

var ipAddress: String {
    get {
        return _ipAddress.isEmpty ? defaultIPAddress : _ipAddress
    }

    set {
        if newValue != _ipAddress {
            _ipAddress = newValue
        }
    }
}
