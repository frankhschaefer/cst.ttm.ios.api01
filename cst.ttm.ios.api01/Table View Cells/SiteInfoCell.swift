//
//  FriendCell.swift
//  TableViewWithMultipleCellTypes
//
//  Created by Stanislav Ostrovskiy on 5/21/17.
//  Copyright © 2017 Stanislav Ostrovskiy. All rights reserved.
//

import UIKit

class SiteInfoCell: UITableViewCell {


    @IBOutlet weak var infoLine1Name: UILabel!
    @IBOutlet weak var infoLine1Value: UILabel!
    
    @IBOutlet weak var infoLine2Name: UILabel!
    @IBOutlet weak var infoLine2Value: UILabel!
    
    
    var item: SettingsViewModelItem? {
        didSet {
            
            guard let item = item as? SettingsViewModelSiteInfoItem else {
                return
            }

            infoLine1Name?.text = "Info - Line 1"
            infoLine1Value?.text = item.infoLine1
            
            infoLine2Name?.text = "Info - Line 2"
            infoLine2Value?.text = item.infoLine2

        }
    }
    
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
}
