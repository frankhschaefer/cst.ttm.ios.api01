//
//  EmailCell.swift
//  TableViewWithMultipleCellTypes
//
//  Created by Stanislav Ostrovskiy on 5/21/17.
//  Copyright © 2017 Stanislav Ostrovskiy. All rights reserved.
//

import UIKit

class TreadDepthCell: UITableViewCell {
        
    @IBOutlet weak var mainTD: UILabel!
    
    @IBOutlet weak var leftRTD: UILabel!

    @IBOutlet weak var middleRTD: UILabel!

    @IBOutlet weak var rightRTD: UILabel!
    
    @IBOutlet weak var tirePosition: UILabel!
    
    @IBOutlet weak var mainConfidence: UILabel!
    @IBOutlet weak var leftConfidence: UILabel!
    @IBOutlet weak var middleConfidence: UILabel!
    @IBOutlet weak var rightConfidence: UILabel!
    
    var item: TireData? {
        didSet {

            guard let theItem = item else {
                return
            }
            
            tirePosition.text = theItem.tirePosition
            
            mainTD.backgroundColor = UIColor().redToGreedColor(value: Double(theItem.mainTD),
                                                                                     vmin: 0,
                                                                                     vmax: 8)

            leftRTD.backgroundColor = UIColor().redToGreedColor(value: Double(theItem.leftRTD),
                                                                                     vmin: 0,
                                                                                     vmax: 8)

            middleRTD.backgroundColor = UIColor().redToGreedColor(value: Double(theItem.middleRTD),
                                                                                     vmin: 0,
                                                                                     vmax: 8)

            rightRTD.backgroundColor = UIColor().redToGreedColor(value: Double(theItem.rightRTD),
                                                                                     vmin: 0,
                                                                                     vmax: 8)

            
            mainTD.text = String(format: "%.1f", theItem.mainTD)
            leftRTD.text = String(format: "%.1f", theItem.leftRTD)
            middleRTD.text = String(format: "%.1f", theItem.middleRTD)
            rightRTD.text = String(format: "%.1f", theItem.rightRTD)

            
            mainConfidence.backgroundColor = UIColor().redToGreedColor(value: Double(theItem.mainCD),
                                                                                     vmin: 0,
                                                                                     vmax: 1000)

            leftConfidence.backgroundColor = UIColor().redToGreedColor(value: Double(theItem.leftCD),
                                                                                     vmin: 0,
                                                                                     vmax: 1000)

            middleConfidence.backgroundColor = UIColor().redToGreedColor(value: Double(theItem.middleCD),
                                                                                     vmin: 0,
                                                                                     vmax: 1000)

            rightConfidence.backgroundColor = UIColor().redToGreedColor(value: Double(theItem.rightCD),
                                                                                     vmin: 0,
                                                                                     vmax: 1000)

            mainConfidence.text = String(theItem.mainCD)
            leftConfidence.text = String(theItem.leftCD)
            middleConfidence.text = String(theItem.middleCD)
            rightConfidence.text = String(theItem.rightCD)

        }
    }

    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
          super.setSelected(selected, animated: animated)

          // Configure the view for the selected state
      }
    

}
