//
//  FriendCell.swift
//  TableViewWithMultipleCellTypes
//
//  Created by Stanislav Ostrovskiy on 5/21/17.
//  Copyright © 2017 Stanislav Ostrovskiy. All rights reserved.
//

import UIKit

class SiteLocationCell: UITableViewCell {


    @IBOutlet weak var line1Name: UILabel!
    @IBOutlet weak var line1Value: UILabel!
    
    @IBOutlet weak var line2Name: UILabel!
    @IBOutlet weak var line2Value: UILabel!
    
    @IBOutlet weak var line3Name: UILabel!
    @IBOutlet weak var line3Value: UILabel!

    @IBOutlet weak var line4Name: UILabel!
    @IBOutlet weak var line4Value: UILabel!

    @IBOutlet weak var line5Name: UILabel!
    @IBOutlet weak var line5Value: UILabel!
    
    
    var item: SettingsViewModelItem? {
        didSet {
            
            guard let item = item as? SettingsViewModelSiteLocationItem else {
                return
            }

            line1Name?.text = "Location Line 1"
            line1Value?.text = item.line1
            
            line2Name?.text = "Location Line 2"
            line2Value?.text = item.line2

            line3Name?.text = "Location Line 3"
            line3Value?.text = item.line3

            line4Name?.text = "Location Line 4"
            line4Value?.text = item.line4

            line5Name?.text = "Location Line 5"
            line5Value?.text = item.line5

        }
    }
    
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
}
