//
//  AttributeCell.swift
//  TableViewWithMultipleCellTypes
//
//  Created by Stanislav Ostrovskiy on 5/21/17.
//  Copyright © 2017 Stanislav Ostrovskiy. All rights reserved.
//

import UIKit

class PlateCell: UITableViewCell {

    @IBOutlet weak var plate: UILabel!
    
    @IBOutlet weak var country: UILabel!
    
    @IBOutlet weak var plateConfidence: UILabel!
    
    @IBOutlet weak var countryConfidence: UILabel!
    
    var item: ProfileViewModelItem?  {
        didSet {
                guard  let item = item as? ProfileViewModelPlateItem else {
                    return
                }
                
                plateConfidence.backgroundColor = UIColor().redToGreedColor(value: Double(item.plateConfidence),
                                                                            vmin: 0,
                                                                            vmax: 100)

                countryConfidence.backgroundColor = UIColor().redToGreedColor(value: Double(item.countryConfidence),
                                                                        vmin: 0,
                                                                        vmax: 100)

                plate.text = item.plate
                country.text = item.country
                plateConfidence.text = String(item.plateConfidence)
                countryConfidence.text = String(item.countryConfidence)

            }
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
