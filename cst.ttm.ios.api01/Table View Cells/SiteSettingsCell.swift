//
//  FriendCell.swift
//  TableViewWithMultipleCellTypes
//
//  Created by Stanislav Ostrovskiy on 5/21/17.
//  Copyright © 2017 Stanislav Ostrovskiy. All rights reserved.
//

import UIKit

class SiteSettingsCell: UITableViewCell {


    @IBOutlet weak var autoPrintName: UILabel!
    @IBOutlet weak var autoPrintValue: UILabel!
    
    @IBOutlet weak var displayDurationOfResultsName: UILabel!
    @IBOutlet weak var displayDurationOfResultsValue: UILabel!
    
    @IBOutlet weak var minimumLimitSummerName: UILabel!
    @IBOutlet weak var minimumLimitSummerValue: UILabel!
    
    @IBOutlet weak var minimumLimitWinterName: UILabel!
    @IBOutlet weak var minimumLimitWinterValue: UILabel!
    
    @IBOutlet weak var recommendedLimitSummerName: UILabel!
    @IBOutlet weak var recommendedLimitSummerValue: UILabel!
    
    @IBOutlet weak var recommendedLimitWinterName: UILabel!
    @IBOutlet weak var recommendedLimitWinterValue: UILabel!

    @IBOutlet weak var unitOfMeasurementForDisplayName: UILabel!
    @IBOutlet weak var unitOfMeasurementForDisplayValue: UILabel!

    @IBOutlet weak var useWinterTireLimitName: UILabel!
    @IBOutlet weak var useWinterTireLimitValue: UILabel!

    @IBOutlet weak var vehicleMovementForDisplayName: UILabel!
    @IBOutlet weak var vehicleMovementForDisplayValue: UILabel!

    
    var item: SettingsViewModelItem? {
        didSet {
            
            guard let item = item as? SettingsViewModelSiteSittingsItem else {
                return
            }

            autoPrintName?.text = "Autoprint"
            autoPrintValue?.text = item.autoPrint
            
            displayDurationOfResultsName?.text = "Display duration of results"
            displayDurationOfResultsValue?.text = item.displayDurationOfResultsValue
            
            minimumLimitSummerName?.text = "Minimum limit summer"
            minimumLimitSummerValue?.text = item.minimumLimitSummerValue
            
            minimumLimitWinterName?.text = "Minimum limit winter"
            minimumLimitWinterValue?.text = item.minimumLimitWinterValue
            
            recommendedLimitSummerName?.text = "Recommended limit summer"
            recommendedLimitSummerValue?.text = item.recommendedLimitSummerValue
            
            recommendedLimitWinterName?.text = "Recommended limit winter"
            recommendedLimitWinterValue?.text = item.recommendedLimitWinterValue
            
            unitOfMeasurementForDisplayName?.text = "Unit of measurement for display"
            unitOfMeasurementForDisplayValue?.text = item.unitOfMeasurementForDisplayValue

            useWinterTireLimitName?.text = "Use winter tire limit"
            useWinterTireLimitValue?.text = item.useWinterTireLimitValue

            vehicleMovementForDisplayName?.text = "Vehicle movement for display"
            vehicleMovementForDisplayValue?.text = item.vehicleMovementForDisplayValue

        }
    }
    
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
}
