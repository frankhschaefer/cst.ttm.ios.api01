//
//  FriendCell.swift
//  TableViewWithMultipleCellTypes
//
//  Created by Stanislav Ostrovskiy on 5/21/17.
//  Copyright © 2017 Stanislav Ostrovskiy. All rights reserved.
//

import UIKit

class MetaCell: UITableViewCell {


    @IBOutlet weak var dataTypeName: UILabel!
    @IBOutlet weak var dataTypeValue: UILabel!
    
    @IBOutlet weak var timeStampName: UILabel!
    @IBOutlet weak var timeStampValue: UILabel!
    
    
    @IBOutlet weak var timeZoneName: UILabel!
    @IBOutlet weak var timeZoneValue: UILabel!
    
    @IBOutlet weak var ttmSerialNumberName: UILabel!
    @IBOutlet weak var ttmSerialNumberValue: UILabel!
    
    @IBOutlet weak var ttmVersionName: UILabel!
    @IBOutlet weak var ttmVersionValue: UILabel!
    
    @IBOutlet weak var unitOfMeasurementName: UILabel!
    @IBOutlet weak var unitOfMeasurementValue: UILabel!
    
    
    var item: SettingsViewModelItem? {
        didSet {
            
            guard let item = item as? SettingsViewModelMetaItem else {
                return
            }

            dataTypeName?.text = "Datatype"
            dataTypeValue?.text = item.dataTypeValue
            
            timeStampName?.text = "Timestamp"
            timeStampValue?.text = item.timeStampValue

            timeZoneName?.text = "Timezone"
            timeZoneValue?.text = item.timeZoneValue

            ttmSerialNumberName?.text = "Serialnumber"
            ttmSerialNumberValue?.text = item.ttmSerialNumberValue

            ttmVersionName?.text = "Version"
            ttmVersionValue?.text = item.ttmVersionValue

            unitOfMeasurementName?.text = "Measurement unit"
            unitOfMeasurementValue?.text = item.unitOfMeasurementValue

        }
    }
    
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
}
