//
//  FirstViewController.swift
//  cst.ttm.ios.api01
//
//  Created by Frank Schäfer on 09.09.19.
//  Copyright © 2019 Frank Schäfer. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    fileprivate let viewModel = ProfileViewModel()

    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView?.tag = 1

        tableView?.dataSource = viewModel

        tableView?.estimatedRowHeight = 100
        tableView?.rowHeight = UITableView.automaticDimension

        tableView?.register(CrossingStatus.nib, forCellReuseIdentifier: CrossingStatus.identifier)
        tableView?.register(NamePictureCell.nib, forCellReuseIdentifier: NamePictureCell.identifier)
        tableView?.register(CrossingDirectionCell.nib, forCellReuseIdentifier: CrossingDirectionCell.identifier)
        tableView?.register(TimeStampCell.nib, forCellReuseIdentifier: TimeStampCell.identifier)
        tableView?.register(PlateCell.nib, forCellReuseIdentifier: PlateCell.identifier)
        tableView?.register(TreadDepthCell.nib, forCellReuseIdentifier: TreadDepthCell.identifier)
    }

}
