//
//  SecondViewController.swift
//  cst.ttm.ios.api01
//
//  Created by Frank Schäfer on 09.09.19.
//  Copyright © 2019 Frank Schäfer. All rights reserved.
//

import SwiftValidator // framework
import UIKit
import Alamofire

class SecondViewController: UIViewController, ValidationDelegate, UITextFieldDelegate {
    /// Validator object for the text fields
    let validator = Validator()

    /// Input field for the IP-Adress / URL
    @IBOutlet var ipAddressTextField: UITextField!

    /// Label above the input field for the IP-Adress / URL
    @IBOutlet var ipAddressLabel: UILabel!

    /// Label for the validation error meassage
    @IBOutlet var IpAddressErrorLabel: UILabel!

    /// Load the UIView
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()

        setTextFields()
    }

    /// called from viewDidLoad
    func initView() {
        // Remove keyboard when a tap outside of the textfield is recorgniced
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))

        // When the rule recorgniced an error, the frame of the textfield will flash in red
        validator.styleTransformers(success: { (validationRule) -> Void in
            debugPrint("clear error label")
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""

            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 0.5
            } else if let textField = validationRule.field as? UITextView {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 0.5
            }
        }, error: { (validationError) -> Void in
            debugPrint("set error label")
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            } else if let textField = validationError.field as? UITextView {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
        })

        // Do any additional setup after loading the view.
        ipAddressTextField.delegate = self

        // Validation Rules are evaluated from left to right.
        validator.registerField(ipAddressTextField,
                                errorLabel: IpAddressErrorLabel,
                                rules: [RequiredRule(), IPV4Rule()])
    }

    func setTextFields() {
        ipAddressTextField.text = ipAddress
    }

    // MARK: - Helper Methods
    
    var alamoFireManager : SessionManager?
    
    func validationSuccessful() {
        debugPrint("Validation Success!")
        
        let alert = UIAlertController(title: "Success",
                                      message: "You are validated!",
                                      preferredStyle: UIAlertController.Style.alert)
        
        let defaultAction = UIAlertAction(title: "OK",
                                          style: .default,
                                          handler: nil)
        alert.addAction(defaultAction)
        
        present(alert, animated: true,
                completion: nil)
    }

    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        debugPrint("Validation FAILED!")
    }

    @objc func hideKeyboard() {
        view.endEditing(true)
    }

    //the json file url
     let URL_TTM_Default = "http://{ip}/api/v3/history?lastnitems=1000";
    
     func getJsonFromUrl(){
        
        // clear the last data from the last request
        dataFromTTM = nil
        // Set Demo-data to valid
        statusDemoLive = true
        
        guard let theIPAdress = ipAddressTextField.text else {
            self.showMsgBox(URL_TTM : "-",
                            info: "No valid IP Adresss")
            return
            
        }
        
        let URL_TTM = URL_TTM_Default.replacingOccurrences(of: "{ip}",
                                                           with: theIPAdress)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 5
        configuration.timeoutIntervalForResource = 5
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        guard let theAlamoFireManager = alamoFireManager else {
            self.showMsgBox(URL_TTM : URL_TTM,
                            info: "The access to the network (WLAN) is not possible!")
            return
        }
        theAlamoFireManager.request(URL_TTM).responseData { (resData) -> Void in
            guard let theValue = resData.result.value,
                  let theResult = String(data : theValue,
                                         encoding : String.Encoding.utf8) else
            {
                self.showMsgBox(URL_TTM : URL_TTM,
                                info: "No result! Check the IP Adresss")
                return
            }
            
            // Transfer the data to the table view
            dataFromTTM = theValue
            
            debugPrint(theResult)
            self.showMsgBox(URL_TTM : URL_TTM,
                            info: theResult)
            
            // Shoe the text "Live"
            statusDemoLive = false
        }
    }
    
    func showMsgBox(URL_TTM : String,
                    info : String)
    {
        // create the alert
        let alert = UIAlertController(title: "JSON Request", message: "Result from \(URL_TTM), \n \(info)", preferredStyle: UIAlertController.Style.alert)

          // add an action (button)
          alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

          // show the alert
          self.present(alert, animated: true, completion: nil)

    }
    
    
    
    /// Handle the Return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        validator.validateField(textField) { error in
            if error == nil {
                // Field validation was successful
                debugPrint("Validation Success, switch to main tab")
                getJsonFromUrl()
            } else {
                // Validation error occurred
                debugPrint("Validation not successfull!")
            }
        }

        return true
    }
}
